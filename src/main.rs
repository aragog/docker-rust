extern crate docker_rust;

use docker_rust::docker::Docker;

fn main() {
    let docker = Docker::new("/var/run/docker.sock").unwrap();

    println!("{:?}", docker.inspect_exec_cmd().ps_args("aux").container_id("cocky_morse").exec())

/*    println!("{:?}", docker.version_cmd().exec());
    println!("{:?}", docker.list_containers_cmd().exec());
    println!("{:?}", docker.stats_cmd().container_id("cocky_morse").stream(false).exec());
    println!("{:?}", docker.inspect_container_cmd().container_id("cocky_morse").size(true).exec());*/
}