use std::collections::HashMap;

/// GET "/containers/json"
#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Container {
    pub Id: String,
    pub Names: Vec<String>,
    pub Image: String,
    pub ImageID: String,
    pub Command: String,
    pub Created: u64,
    pub Ports: Vec<Port>,
    pub SizeRw: Option<i64>,
    pub SizeRootFs: Option<i64>,
    pub Labels: HashMap<String, String>,
    pub State: String,
    pub Status: String,
    pub HostConfig: HostConfig,
    pub NetworkSettings: Option<SummaryNetworkSettings>,
    pub Mounts: Vec<MountPoint>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Port {
    pub IP: Option<String>,
    pub PrivatePort: u16,
    pub PublicPort: Option<u16>,
    pub Type: String
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct HostConfig {
    pub NetworkMode: Option<String>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct SummaryNetworkSettings {
    pub Networks: HashMap<String, Option<EndpointSettings>>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct EndpointSettings {
    pub IPAMConfig: Option<EndpointIPAMConfig>,
    pub Links: Option<String>,
    pub Aliases: Option<String>,
    pub NetworkID: String,
    pub EndpointID: String,
    pub Gateway: String,
    pub IPAddress: String,
    pub IPPrefixLen: isize,
    pub IPv6Gateway: String,
    pub GlobalIPv6Address: String,
    pub GlobalIPv6PrefixLen: isize,
    pub MacAddress: String
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct EndpointIPAMConfig {
    pub IPv4Address: Option<String>,
    pub IPv6Address: Option<String>,
    pub LinkLocalIPs: Vec<String>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct MountPoint {
    pub Type: Option<String>,
    pub Name: Option<String>,
    pub Source: String,
    pub Destination: String,
    pub Driver: Option<String>,
    pub Mode: String,
    pub RW: bool,
    pub Propagation: String
}

/// ContainerState stores container's running state
/// it's part of ContainerJSONBase and will return by "inspect" command
#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct ContainerJSONBase {
    pub Id: String,
    pub Created: String,
    pub Path: String,
    pub Args: Vec<String>,
    pub State: Option<ContainerState>,
    pub Image: String,
    pub ResolvConfPath:String,
    pub HostnamePath: String,
    pub HostsPath: String,
    pub LogPath: String,
    pub Node: Option<ContainerNode>,
    pub Name: String,
    pub RestartCount: isize,
    pub Driver: String,
    pub MountLabel: String,
    pub ProcessLabel: String,
    pub AppArmorProfile: String,
    pub ExecIDs: Option<Vec<String>>,
    pub HostConfig: Option<HostConfigNonPort>,
    pub GraphDriver: Option<GraphDriverData>,
    pub SizeRw: Option<i64>,
    pub SizeRootFs: Option<i64>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct ContainerState {
    pub Status: String,
    pub Running: bool,
    pub Paused: bool,
    pub Restarting: bool,
    pub OOMKilled: bool,
    pub Dead: bool,
    pub Pid: isize,
    pub ExitCode: isize,
    pub Error: String,
    pub StartedAt: String,
    pub FinishedAt: String,
    pub Health: Option<Health>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Health {
    pub Status: String,
    pub FailingStreak: isize,
    pub Log: Option<Vec<HealthcheckResult>>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct HealthcheckResult {
    pub Start: String,
    pub End: String,
    pub ExitCode: isize,
    pub Output: String
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct ContainerNode {
    pub ID: String,
    pub IP: String,
    pub Addr: String,
    pub Name: String,
    pub Cpus: isize,
    pub Memory: i64,
    pub Labels: HashMap<String, String>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct HostConfigNonPort {
    pub Binds: Option<Vec<String>>,
    pub ContainerIDFile: String,
    pub LogConfig: LogConfig,
    pub NetworkMode: String,
    pub PortBindings: HashMap<String, Vec<PortBinding>>,
    pub RestartPolicy: RestartPolicy,
    pub AutoRemove: bool,
    pub VolumeDriver: String,
    pub VolumesFrom: Option<Vec<String>>,
    pub CapAdd: Option<Vec<String>>,
    pub CapDrop: Option<Vec<String>>,
    pub Dns: Option<Vec<String>>,
    pub DnsOptions: Option<Vec<String>>,
    pub DnsSearch: Option<Vec<String>>,
    pub ExtraHosts: Option<Vec<String>>,
    pub GroupAdd: Option<Vec<String>>,
    pub IpcMode: Option<String>,
    pub Cgroup: Option<String>,
    pub Links: Option<Vec<String>>,
    pub OomScoreAdj: Option<isize>,
    pub PidMode: Option<String>,
    pub Privileged: Option<bool>,
    pub PublishAllPorts: Option<bool>,
    pub ReadonlyRootfs: Option<bool>,
    pub SecurityOpt: Option<Vec<String>>,
    pub StorageOpt: Option<HashMap<String, String>>,
    pub Tmpfs: Option<HashMap<String, String>>,
    pub UTSMode: Option<String>,
    pub UsernsMode: Option<String>,
    pub ShmSize: Option<i64>,
    pub Sysctls: Option<HashMap<String, String>>,
    pub Runtime: Option<String>,
    pub ConsoleSize: Option<Vec<usize>>,
    pub Isolation:Option<String>,
    pub CpuShares: i64,
    pub Memory: i64,
    pub NanoCpus: Option<i64>,
    pub CgroupParent: Option<String>,
    pub BlkioWeight: Option<u16>,
    pub BlkioWeightDevice: Option<Vec<WeightDevice>>,
    pub BlkioDeviceReadBps: Option<Vec<ThrottleDevice>>,
    pub BlkioDeviceWriteBps: Option<Vec<ThrottleDevice>>,
    pub BlkioDeviceReadIOps: Option<Vec<ThrottleDevice>>,
    pub BlkioDeviceWriteIOps: Option<Vec<ThrottleDevice>>,
    pub CpuPeriod: Option<i64>,
    pub CpuQuota: Option<i64>,
    pub CpuRealtimePeriod: Option<i64>,
    pub CpuRealtimeRuntime: Option<i64>,
    pub CpusetCpus: Option<String>,
    pub CpusetMems: Option<String>,
    pub Devices: Option<Vec<DeviceMapping>>,
    pub DiskQuota: Option<i64>,
    pub KernelMemory: Option<i64>,
    pub MemoryReservation: Option<i64>,
    pub MemorySwap: Option<i64>,
    pub MemorySwappiness: Option<i64>,
    pub OomKillDisable: Option<bool>,
    pub PidsLimit: Option<i64>,
    pub Ulimits: Option<Vec<Ulimit>>,
    pub CpuCount: Option<i64>,
    pub CpuPercent: Option<i64>,
    pub IOMaximumIOps: Option<u64>,
    pub IOMaximumBandwidth: Option<u64>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct LogConfig {
    pub Type: String,
    pub Config: HashMap<String, String>
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct PortBinding {
    pub HostIp: String,
    pub HostPort: String
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct RestartPolicy {
    pub Name: String,
    pub MaximumRetryCount: isize
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct WeightDevice {
    pub Path: String,
    pub Weight: u16
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct ThrottleDevice {
    pub Path: String,
    pub Rate: u64
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct DeviceMapping {
    pub PathOnHost: String,
    pub PathInContainer: String,
    pub CgroupPermissions: String
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Ulimit {
    pub Name: String,
    pub Hard: i64,
    pub Soft: i64
}

#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct GraphDriverData {
    pub Name: Option<String>,
    pub Data: Option<HashMap<String, String>>
}

/// ContainerProcessList contains response of Engine API:
/// GET "/containers/{name:.*}/top"
#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct ContainerProcessList {
    pub Processes: Vec<Vec<String>>,
    pub Titles: Vec<String>
}