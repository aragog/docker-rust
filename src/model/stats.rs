use std::collections::HashMap;

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct Stats {
    pub read: String,
    pub preread: Option<String>,
    pub pids_stats: Option<PidStats>,
    pub networks: Option<HashMap<String, Network>>,
    pub memory_stats: Option<MemoryStats>,
    pub blkio_stats: Option<HashMap<String, Vec<BlkioStatsInfo>>>,
    pub num_procs: Option<u32>,
    pub storage_stats: Option<StorageStats>,
    pub cpu_stats: Option<CpuStats>,
    pub precpu_stats: Option<CpuStats>
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct PidStats {
    pub current: Option<u64>,
    pub limit: Option<u64>
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct Network {
    pub rx_dropped: u64,
    pub rx_bytes: u64,
    pub rx_errors: Option<u64>,
    pub tx_packets: u64,
    pub tx_dropped: u64,
    pub rx_packets: u64,
    pub tx_errors: Option<u64>,
    pub tx_bytes: u64,
    pub endpoint_id: Option<String>,
    pub instance_id: Option<String>
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct MemoryStats {
    pub stats: Option<StatsInfo>,
    pub max_usage: Option<u64>,
    pub usage: Option<u64>,
    pub failcnt: Option<u64>,
    pub limit: Option<u64>,
    pub commitbytes: Option<u64>,
    pub commitpeakbytes: Option<u64>,
    pub privateworkingset: Option<u64>
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct StatsInfo {
    pub total_pgmajfault: u64,
    pub cache: u64,
    pub mapped_file: u64,
    pub total_inactive_file: u64,
    pub pgpgout: u64,
    pub rss: u64,
    pub total_mapped_file: u64,
    pub writeback: u64,
    pub unevictable: u64,
    pub pgpgin: u64,
    pub total_unevictable: u64,
    pub pgmajfault: u64,
    pub total_rss: u64,
    pub total_rss_huge: u64,
    pub total_writeback: u64,
    pub total_inactive_anon: u64,
    pub rss_huge: u64,
    pub hierarchical_memory_limit: u64,
    pub total_pgfault: u64,
    pub total_active_file: u64,
    pub active_anon: u64,
    pub total_active_anon: u64,
    pub total_pgpgout: u64,
    pub total_cache: u64,
    pub inactive_anon: u64,
    pub active_file: u64,
    pub pgfault: u64,
    pub inactive_file: u64,
    pub total_pgpgin: u64
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct BlkioStatsInfo {
    pub major: u64,
    pub minor: u64,
    pub op: String,
    pub value: u64
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct CpuStats {
    pub cpu_usage: CpuUsage,
    pub system_cpu_usage: Option<u64>,
    pub throttling_data: Option<ThrottlingData>
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct CpuUsage {
    pub percpu_usage: Option<Vec<u64>>,
    pub usage_in_usermode: u64,
    pub total_usage: u64,
    pub usage_in_kernelmode: u64,
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct ThrottlingData {
    periods: u64,
    throttled_periods: u64,
    throttled_time: u64
}

#[derive(RustcEncodable, RustcDecodable)]
#[derive(Debug)]
pub struct StorageStats {
    pub read_count_normalized: Option<u64>,
    pub read_size_bytes: Option<u64>,
    pub write_count_normalized: Option<u64>,
    pub write_size_bytes: Option<u64>
}