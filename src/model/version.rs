/// GET "/version"
#[derive(RustcEncodable, RustcDecodable)]
#[allow(non_snake_case)]
#[derive(Debug)]
pub struct Version {
    pub Version: String,
    pub ApiVersion: String,
    pub MinAPIVersion: Option<String>,
    pub GitCommit: String,
    pub GoVersion: String,
    pub Os: String,
    pub Arch: String,
    pub KernelVersion: Option<String>,
    pub BuildTime: Option<String>,
    pub Experimental: Option<bool>
}