use std::collections::HashMap;
use url::form_urlencoded::Serializer as UrlSerializer;

pub trait Serializer {
    fn params(&self) -> &HashMap<&'static str, String>;

    fn serialize(&mut self) -> String {
        let mut serializer = UrlSerializer::new(String::new());
        for (key, param) in self.params() {
            serializer.append_pair(key, param);
        }
        serializer.finish()
    }
}