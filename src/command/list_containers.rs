use rustc_serialize::json;

use std::io::Result;
use std::collections::HashMap;

use docker::Docker;
use serializer::Serializer;
use model::container::Container;

/// Filter options for container listings
pub enum ContainerFilter<'a> {
    ExitCode(i64),
    Status(&'a str),
    LabelName(&'a str),
    Label(&'a str, &'a str),
    Isolation(&'a str),
    Ancestor(&'a str),
    Before(&'a str),
    Since(&'a str),
    Volume(&'a str)
}

pub struct ListContainersCmd<'a> {
    docker: &'a Docker,
    params: HashMap<&'static str, String>
}

impl<'a> Serializer for ListContainersCmd<'a> {
    fn params(&self) -> &HashMap<&'static str, String> {
        &self.params
    }
}

impl<'a> ListContainersCmd<'a> {
    pub fn new(docker: &Docker) -> ListContainersCmd {
        ListContainersCmd {
            docker: docker,
            params: HashMap::new()
        }
    }

    pub fn all(&mut self, all: bool) -> &mut ListContainersCmd<'a> {
        self.params.insert("all", all.to_string());
        self
    }

    pub fn limit(&mut self, limit: u64) -> &mut ListContainersCmd<'a> {
        self.params.insert("limit", limit.to_string());
        self
    }

    pub fn before(&mut self, before: &str) -> &mut ListContainersCmd<'a> {
        self.params.insert("before", before.to_string());
        self
    }

    pub fn since(&mut self, since: &str) -> &mut ListContainersCmd<'a> {
        self.params.insert("since", since.to_string());
        self
    }

    pub fn size(&mut self, size: bool) -> &mut ListContainersCmd<'a> {
        self.params.insert("size", size.to_string());
        self
    }

    pub fn filter(&mut self, filters: &Vec<ContainerFilter>) -> &mut ListContainersCmd<'a> {
        let mut params = HashMap::new();
        for f in filters {
            match f {
                &ContainerFilter::ExitCode(c) => params.insert("exited", vec!(c.to_string())),
                &ContainerFilter::Status(s) => params.insert("status", vec!(s.to_string())),
                &ContainerFilter::LabelName(l) => params.insert("label", vec!(l.to_string())),
                &ContainerFilter::Label(l, v) => params.insert("label", vec!(format!("{}={}", l, v))),
                &ContainerFilter::Ancestor(id) => params.insert("ancestor", vec!(id.to_string())),
                &ContainerFilter::Before(id) => params.insert("before", vec!(id.to_string())),
                &ContainerFilter::Since(id) => params.insert("since", vec!(id.to_string())),
                &ContainerFilter::Isolation(i) => params.insert("isolation", vec!(i.to_string())),
                &ContainerFilter::Volume(v) => params.insert("volume", vec!(v.to_string()))
            };
        }
        self.params.insert("filters", json::encode(&params).unwrap());
        self
    }

    pub fn exec(&mut self) -> Result<Vec<Container>> {
        let req = format!("/containers/json?{}", self.serialize());
        let resp = self.docker.request(&req);
        let containers: Vec<Container> = match json::decode(&resp) {
            Ok(body) => body,
            Err(e) => panic!(e)
        };
        return Ok(containers);
    }
}