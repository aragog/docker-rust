use rustc_serialize::json;

use std::io::Result;

use docker::Docker;
use model::version::Version;

pub struct VersionCmd<'a> {
    docker: &'a Docker
}

impl<'a> VersionCmd<'a> {
    pub fn new(docker: &Docker) -> VersionCmd {
        VersionCmd {
            docker: docker,
        }
    }

    pub fn exec(&self) -> Result<Version> {
        let resp = self.docker.request("/version");
        let version: Version = match json::decode(&resp) {
            Ok(body) => body,
            Err(e) => panic!(e)
        };
        return Ok(version);
    }
}