use rustc_serialize::json;

use std::io::Result;
use std::collections::HashMap;

use docker::Docker;
use serializer::Serializer;
use model::stats::Stats;

pub struct StatsCmd<'a> {
    docker: &'a Docker,
    container_id: Option<&'a str>,
    params: HashMap<&'static str, String>
}

impl<'a> Serializer for StatsCmd<'a> {
    fn params(&self) -> &HashMap<&'static str, String> {
        &self.params
    }
}

impl<'a> StatsCmd<'a> {
    pub fn new(docker: &Docker) -> StatsCmd {
        StatsCmd {
            docker: docker,
            container_id: None,
            params: HashMap::new()
        }
    }

    pub fn stream(&mut self, stream: bool) -> &mut StatsCmd<'a> {
        self.params.insert("stream", stream.to_string());
        self
    }

    pub fn container_id(&mut self, container_id: &'a str) -> &mut StatsCmd<'a> {
        self.container_id = Some(container_id);
        self
    }

    pub fn exec(&mut self) -> Result<Stats> {
        let id = match self.container_id {
            Some(id) => id,
            None => panic!("Container ID required")
        };
        let req = format!("/containers/{}/stats?{}", id, self.serialize());
        let resp = self.docker.request(&req);
        let stats: Stats = match json::decode(&resp) {
            Ok(body) => body,
            Err(e) => panic!(e)
        };
        return Ok(stats);
    }
}