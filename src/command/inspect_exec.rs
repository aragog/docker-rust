use rustc_serialize::json;

use std::collections::HashMap;
use std::io::Result;

use docker::Docker;
use model::container::ContainerProcessList;
use serializer::Serializer;

pub struct InspectExecCmd<'a> {
    docker: &'a Docker,
    container_id: Option<&'a str>,
    params: HashMap<&'static str, String>
}

impl<'a> Serializer for InspectExecCmd<'a> {
    fn params(&self) -> &HashMap<&'static str, String> {
        &self.params
    }
}

impl<'a> InspectExecCmd<'a> {
    pub fn new(docker: &Docker) -> InspectExecCmd {
        InspectExecCmd {
            docker: docker,
            container_id: None,
            params: HashMap::new()
        }
    }

    pub fn container_id(&mut self, container_id: &'a str) -> &mut InspectExecCmd<'a> {
        self.container_id = Some(container_id);
        self
    }

    pub fn ps_args(&mut self, args: &str) -> &mut InspectExecCmd<'a> {
        self.params.insert("ps_args", args.to_string());
        self
    }

    pub fn exec(&mut self) -> Result<ContainerProcessList> {
        let id = match self.container_id {
            Some(id) => id,
            None => panic!("Container ID required")
        };
        let req = format!("/containers/{}/top?{}", id, self.serialize());
        let resp = self.docker.request(&req);
        let cpl: ContainerProcessList = match json::decode(&resp) {
            Ok(body) => body,
            Err(e) => panic!(e)
        };
        return Ok(cpl);
    }
}