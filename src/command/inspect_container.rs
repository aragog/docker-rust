use rustc_serialize::json;

use std::io::Result;
use std::collections::HashMap;

use docker::Docker;
use serializer::Serializer;
use model::container::ContainerJSONBase;

pub struct InspectContainerCmd<'a> {
    docker: &'a Docker,
    container_id: Option<&'a str>,
    params: HashMap<&'static str, String>
}

impl<'a> Serializer for InspectContainerCmd<'a> {
    fn params(&self) -> &HashMap<&'static str, String> {
        &self.params
    }
}

impl<'a> InspectContainerCmd<'a> {
    pub fn new(docker: &Docker) -> InspectContainerCmd {
        InspectContainerCmd {
            docker: docker,
            container_id: None,
            params: HashMap::new()
        }
    }

    pub fn size(&mut self, stream: bool) -> &mut InspectContainerCmd<'a> {
        self.params.insert("size", stream.to_string());
        self
    }

    pub fn container_id(&mut self, container_id: &'a str) -> &mut InspectContainerCmd<'a> {
        self.container_id = Some(container_id);
        self
    }

    pub fn exec(&mut self) -> Result<ContainerJSONBase> {
        let id = match self.container_id {
            Some(id) => id,
            None => panic!("Container ID required")
        };
        let req = format!("/containers/{}/json?{}", id, self.serialize());
        let resp = self.docker.request(&req);
        let container: ContainerJSONBase = match json::decode(&resp) {
            Ok(body) => body,
            Err(e) => panic!(e)
        };
        return Ok(container);
    }
}