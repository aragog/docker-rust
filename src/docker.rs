use std::io::Result;

use hyperlocal::UnixSocketConnector;
use hyper::Client;
use hyper::method::Method;

use transport::Transport;
use command::VersionCmd;
use command::StatsCmd;
use command::ListContainersCmd;
use command::InspectContainerCmd;
use command::InspectExecCmd;

pub struct Docker {
    transport: Transport
}

impl Docker {
    pub fn new(socket: &str) -> Result<Docker> {
        Ok(Docker {
            transport: Transport::Unix {
                client: Client::with_connector(UnixSocketConnector),
                path: socket.to_string()
            }
        })
    }

    pub fn version_cmd(&self) -> VersionCmd {
        VersionCmd::new(self)
    }

    pub fn stats_cmd(&self) -> StatsCmd {
        StatsCmd::new(self)
    }

    pub fn list_containers_cmd(&self) -> ListContainersCmd {
        ListContainersCmd::new(self)
    }

    pub fn inspect_container_cmd(&self) -> InspectContainerCmd {
        InspectContainerCmd::new(self)
    }

    pub fn inspect_exec_cmd(&self) ->InspectExecCmd {
        InspectExecCmd::new(self)
    }

    pub fn request(&self, path: &str) -> String {
        self.transport.request(Method::Get, path)
    }
}