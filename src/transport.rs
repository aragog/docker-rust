use std::io::Read;

use hyper::Client;
use hyper::method::Method;
use hyperlocal::DomainUrl;

pub enum Transport {
    /// A Unix domain socket
    Unix {
        client: Client,
        path: String,
    }
}

impl Transport {
    pub fn request(&self, method: Method, endpoint: & str) -> String {
        let req = match *self {
            Transport::Unix {  ref client, ref path } => {
                client.request(method, DomainUrl::new(&path, endpoint))
            }
        };
        let res = req.send();
        let mut body = Vec::new();
        res.unwrap().read_to_end(&mut body).unwrap();
        String::from_utf8(body).unwrap()
    }
}